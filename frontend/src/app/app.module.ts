import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module'; // Import AppRoutingModule
import { HttpClientModule } from '@angular/common/http'; 
import { AppComponent } from './app.component';
import { TodoListComponent } from './todo-list/todo-list.component';

@NgModule({
  declarations: [AppComponent, TodoListComponent],
  imports: [BrowserModule, FormsModule,HttpClientModule, AppRoutingModule], // Include AppRoutingModule in imports
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

