import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css'],
})
export class TodoListComponent implements OnInit {
  todos: any[] = [];
  newTodo: any = {};
  isDarkMode: boolean = false;
  constructor(private todoService: TodoService) {}

  ngOnInit() {
    this.getTodos();
  }

  getTodos() {
    this.todoService.getAllTodos().subscribe((data) => {
      this.todos = data;
    });
  }

  addTodo() {
    this.todoService.addTodo(this.newTodo).subscribe((data) => {
      this.todos.push(data);
      this.newTodo = {};
    });
  }

  updateTodo(todo: any) {
    this.todoService.updateTodo(todo).subscribe((data) => {
      // Update the local todo item if needed
    });
  }

  deleteTodo(id: number) {
    this.todoService.deleteTodo(id).subscribe(() => {
      this.todos = this.todos.filter((todo) => todo.id !== id);
    });
  }
}
