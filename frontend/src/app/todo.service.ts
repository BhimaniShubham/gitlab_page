import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TodoService {
  private baseUrl = 'http://localhost:3000/api/todos'; // Replace with your server API URL

  constructor(private http: HttpClient) {}

  getAllTodos(): Observable<any[]> {
    return this.http.get<any[]>(this.baseUrl);
  }

  addTodo(todo: any): Observable<any> {
    return this.http.post<any>(this.baseUrl, todo);
  }

  updateTodo(todo: any): Observable<any> {
    const url = `${this.baseUrl}/${todo.id}`;
    return this.http.put<any>(url, todo);
  }

  deleteTodo(id: number): Observable<any> {
    const url = `${this.baseUrl}/${id}`;
    return this.http.delete<any>(url);
  }
}
